#pragma once
#include <stdexcept>
#include <format>
#include <optional>

namespace generic
{
    template<typename __TypeList, template<typename> class __Prototype>
    class Any
    {
    private:
        void* __ptr;
        std::size_t __myIdx;

        template<typename T, std::size_t idx = __TypeList::Size, typename = void>
        struct try_cast
        {
            static __Prototype<T>* __get_ptr__(void* ptr, const std::size_t& myIdx)
            {
                if ((std::is_base_of_v<T, __TypeList::template Nth<idx>> || std::is_convertible_v<__TypeList::template Nth<idx>, T>) && idx == myIdx)
                {
                    return reinterpret_cast<__Prototype<T>*>(ptr);
                }
                else
                    return Any<__TypeList, __Prototype>::template try_cast<T, idx - 1U>::__get_ptr__(ptr, myIdx);
            }

            static __Prototype<T>& __get_ref__(void* ptr, const std::size_t& myIdx)
            {
                if ((std::is_base_of_v<T, __TypeList::template Nth<idx>> || std::is_convertible_v<__TypeList::template Nth<idx>, T>) && idx == myIdx)
                    return *reinterpret_cast<__Prototype<T>*>(ptr);
                else
                    return Any<__TypeList, __Prototype>::template try_cast<T, idx - 1U>::__get_ref__(ptr, myIdx);
            }
        };

        template<typename T, std::size_t idx>
        struct try_cast<T, idx, std::enable_if_t<idx == 0U>>
        {
            static __Prototype<T>* __get_ptr__(const void* ptr, const std::size_t& myIdx)
            {
                return nullptr;
            }

            static __Prototype<T>& __get_ref__(const void* ptr, const std::size_t& myIdx)
            {
                throw std::runtime_error(std::format("Invalid conversion to %s.", typeid(T).name()).c_str());
            }
        };

    public:
        Any() = delete;

        Any(const auto* ptr, const std::size_t& tidx)
            : __ptr((void*)ptr)
            , __myIdx(ptr ? tidx : 0U)
        {}

        Any(const Any& other)
            : __ptr(other.__ptr)
            , __myIdx(other.__myIdx)
        {}

        Any(Any&& other)
            : __ptr(other.__ptr)
            , __myIdx(other.__myIdx)
        {
            other.__ptr = nullptr;
            other.__myIdx = 0U;
        }

        ~Any()
        {
            __ptr = nullptr;
        }

        bool has_value() const
        {
            return __ptr;
        }

        void reset()
        {
            __ptr = nullptr;
        }

        template<typename T>
        auto& operator=(const T& other)
        {
            if constexpr (std::is_same_v<std::decay_t<T>, Any>)
            {
                this->__ptr = other.__ptr;
                this->__myIdx = other.__myIdx;
            }
            else
            {
                auto ptr = try_cast<T>::__get_ptr__(__ptr, __myIdx);
                if (ptr)
                    *ptr = other;
            }
            
            return *this;
        }

        template<typename T>
        auto& operator=(T&& other)
        {
            if constexpr (std::is_same_v<std::decay_t<T>, Any>)
            {
                this->__ptr = other.__ptr;
                this->__myIdx = other.__myIdx;
                other.__ptr = nullptr;
                other.__myIdx = 0U;
            }
            else
            {
                auto ptr = try_cast<T>::__get_ptr__(__ptr, __myIdx);
                if (ptr)
                    *ptr = std::move(other);
            }
            
            return *this;
        }
        
        /**
        * @brief Throws if value cannot be assigned.
        */
        template<typename T>
        auto& assign(const T& other)
        {
            try_cast<T>::__get_ref__(__ptr, __myIdx) = other;
            return *this;
        }

        template<typename T>
        auto& assign(T&& other)
        {
            try_cast<T>::__get_ref__(__ptr, __myIdx) = std::move(other);
            return *this;
        }

        void* data()
        {
            return __ptr;
        }

        const void* data() const
        {
            return __ptr;
        }

        /**
        * @brief Throws if cast is not possible.
        */
        template<typename T>
        auto& cast()
        {
            return try_cast<T>::__get_ref__(__ptr, __myIdx);
        }

        template<typename T>
        const auto& cast() const
        {
            return try_cast<T>::__get_ref__(__ptr, __myIdx);
        }

        template<typename T>
        auto safe_cast()
        {
            auto p = try_cast<T>::__get_ptr__(__ptr, __myIdx);
            return p ? std::optional<std::reference_wrapper<__Prototype<T>>>(*p) : std::nullopt;
        }

        template<typename T>
        const auto safe_cast() const
        {
            auto p = try_cast<T>::__get_ptr__(__ptr, __myIdx);
            return p ? std::optional<std::reference_wrapper<__Prototype<T>>>(*p) : std::nullopt;
        }

    };

}