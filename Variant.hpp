#pragma once
#include "TypeList.hpp"
#include "Container.hpp"
#include "TypeListFunctions.hpp"

namespace generic
{
    /**
    * @brief Specialization of Container.
    */
    template<typename __TypeList, typename __Base = NullType, std::size_t nptr = 1U, template<typename> class __Prototype = NullPrototype>
    class Variant;

    template<typename __TypeList, typename __Base, std::size_t nptr, template<typename> class __Prototype>
    class Variant
        : public Container<__TypeList, typename __TypeList::IndexType, nptr, __Prototype, __Base>
    {
    protected:
        using __ThisType = Variant<__TypeList, __Base, nptr>;
        using __UnderlyingType = Container<__TypeList, typename __TypeList::IndexType, nptr, __Prototype, __Base>;

    public:
        Variant()
            : __UnderlyingType()
        {}

        virtual ~Variant()
        {}

        Variant(const __ThisType& other)
            : __UnderlyingType(static_cast<const __UnderlyingType&>(other))
        {}

        Variant(__ThisType&& other) noexcept
            : __UnderlyingType(static_cast<__UnderlyingType&&>(std::forward<__ThisType>(other)))
        {}

        __ThisType& operator=(const __ThisType& other)
        {
            __UnderlyingType::operator=(static_cast<const __UnderlyingType&>(other));
            return *this;
        }

        bool operator==(const __ThisType& other) const
        {
            return __UnderlyingType::operator==(static_cast<const __UnderlyingType&>(other));
        }

        void clear()
        {
            __UnderlyingType::clear();
        }
    };

}