#pragma once
#include "TypeList.hpp"
#include <type_traits>
#include <functional>

namespace generic
{
    /**
    * @brief Polymorfic 'for'.
    * @tparam T is a TypeList
    */
    template<typename T, std::size_t Idx = T::Size, typename = void>
    struct For;

    template<typename T, std::size_t Idx, typename>
    struct For
    {
        static_assert(is_type_list<T>::value, "T must be a TypeList.");

        /**
        * @brief Maps a function to each type the object contains.
        * @param[in] obj A TypeList object
        * @param[in] l A templated lambda with one argument (the object of type T), e.g., []<typename T>(T*& t) {}
        */
        template<typename K, typename L>
        static void map(K& obj, L l)
        {
            l(obj.get<T::template Nth<Idx>>());
            For<T, Idx - 1U>::template map<K, L>(obj, l);
        }

        template<typename U>
        struct in 
        {
            /**
            * @brief Maps a function to each type declared in U the object contains.
            * @param[in] obj A TypeList object
            * @param[in] l A templated lambda with one argument (the object of type T), e.g., []<typename T>(T*& t) {}
            */
            template<typename K, typename L>
            static void map(K& obj, L l)
            {
                if constexpr (U::template contains<T::template Nth<Idx>>::value)
                    l(obj.get<T::template Nth<Idx>>());
                For<T, Idx - 1U>::template in<U>:: template map<K, L>(obj, l);
            }
        };
    };

    template<typename T, std::size_t Idx>
    struct For<T, Idx, typename std::enable_if_t<Idx == 0U>>
    {
        static_assert(is_type_list<T>::value, "T must be a TypeList.");

        template<typename K, typename L>
        static void map(K& obj, L l) {}

        template<typename U>
        struct in
        {
            template<typename K, typename L>
            static void map(K& obj, L l) {}
        };
    };


    /*
    struct dereference
    {
        template<typename T>
        underlying_pointer_t<T>& from(T& ptr)
        {
            if constexpr (std::is_pointer_v<T>)
            {
                if (ptr) {
                    dereference::template from<T>
                }
            }
            else
            {

            }
        }
    };

    template<typename T, std::size_t Idx = T::Size, typename = void>
    struct Invoke
    {
        template<typename Interface, typename = void>
        struct with
            : Interface
            , T::template Nth<Idx>
        {
            using __ThisType = Invoke<T, Idx>::template with<Interface>;

            template<typename E, typename F>
            static void call(E& obj, F method)
            {
                using __LocalType = typename T::template Nth<Idx>;
                auto val = reinterpret_cast<__ThisType*>(obj.get<__LocalType>());
                auto call = std::bind(method, val);
                call();
                Invoke<T, Idx - 1U>::template with<Interface>::call(obj, method);
            }
        };

        template<typename Interface>
        struct with<Interface, typename std::enable_if_t<std::is_base_of_v<Interface, typename T::template Nth<Idx>>>>
            : T::template Nth<Idx>
        {
            using __ThisType = Invoke<T, Idx>::template with<Interface>;

            template<typename E, typename F>
            static void call(E& obj, F method)
            {
                using __LocalType = typename T::template Nth<Idx>;
                auto call = std::bind(method, obj.get<__LocalType>());
                call();
                Invoke<T, Idx - 1U>::template with<Interface>::call(obj, method);
            }
        };
    };

    template<typename T, std::size_t Idx>
    struct Invoke<T, Idx, typename std::enable_if_t<Idx == 0U>>
    {
        template<typename Interface>
        struct with
            : T::template Nth<Idx>
            , Interface
        {
            template<typename E, typename F>
            static void call(E & obj, F method)
            {}
        };
    };
    */

}


