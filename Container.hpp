#pragma once
#include "Any.hpp"
#include "TypeList.hpp"
#include <stdexcept>

namespace generic
{
    /**
    * @brief Null prototype.
    */
    template<typename T>
    using NullPrototype = T;

    /**
    * @brief Container for type listed data meant for supporting other data types.
    * @tparam T is a TypeList.
    * @tparam IndexType Type for indexing underlying types (e.g., enum).
    * @tparam nptr Pointer indirection the data types will be declared with.
    * @tparam Prototype template expecting one type parameter, with which each type in the type list will be declared.
    * @tparam CommonBase A base which will enable fetching types at runtime.
    */
	template
        < typename __TypeList
        , typename IndexType = std::size_t
        , std::size_t nptr = 1U
        , template<typename> class Prototype = NullPrototype
        , typename CommonBase = NullType
        , typename = void
        >
    class Container;

    template
        < typename T
        , std::size_t Idx = T::Size
        , std::size_t nptr = 1U
        , template<typename> class Prototype = NullPrototype
        , typename = void
        >
    class ContainerTypes;

    template
        < typename __TypeList
        , typename IndexType
        , std::size_t nptr
        , template<typename> class Prototype
        , typename CommonBase
        >
    class Container
        < __TypeList, IndexType
        , nptr, Prototype, CommonBase
        , std::enable_if_t<std::is_same_v<CommonBase, NullType> || is_common_base_v<__TypeList, CommonBase>>
        >
         : private ContainerTypes<__TypeList, __TypeList::Size, nptr>
    {
    private:
        static_assert(is_type_list_v<__TypeList>, "__TypeList must be a TypeList.");

        template<typename __Ty>
        using make_type_t = make_ptr_t<Prototype<__Ty>, nptr>;

        using __ThisType = Container<__TypeList, IndexType, nptr, Prototype, CommonBase>;
        using __UnderlyingType = ContainerTypes<__TypeList, __TypeList::Size, nptr, Prototype>;

    public:
        Container() 
            : __UnderlyingType()
        {}

        Container(const __ThisType& other)
            : __UnderlyingType(static_cast<const __UnderlyingType&>(other))
        {}

        Container(__ThisType&& other)
            : __UnderlyingType(static_cast<__UnderlyingType&&>(std::forward<__ThisType>(other)))
        {}

        virtual ~Container()
        {}

        __ThisType& operator=(const __ThisType& other)
        {
            __UnderlyingType::operator=(static_cast<const __UnderlyingType&>(other));
            return *this;
        }

        bool operator==(const __ThisType& other) const
        {
            return __UnderlyingType::operator==(other);
        }

        /**
        * @brief Get by index provided a base.
        * @tparam S Base 
        */
        template<typename S = CommonBase, typename = std::enable_if_t<is_common_base_v<__TypeList, S>>>
        make_type_t<S>& get(const IndexType& idx)
        {
            return *reinterpret_cast<make_type_t<S>*>(
                __UnderlyingType::__pointer__(static_cast<std::size_t>(idx)));
        }

        template<typename S = CommonBase, typename = void>
        const make_type_t<S>& get(const IndexType& idx) const
        {
            return *reinterpret_cast<const make_type_t<S>*>(
                __UnderlyingType::__pointer__(static_cast<std::size_t>(idx)));
        }

        /**
        * @brief Get by index without casting to a base.
        */
        auto get(const IndexType& idx)
        {
            return Any<__TypeList, make_type_t>(
                __UnderlyingType::__pointer__(static_cast<std::size_t>(idx)), idx);
        }

        const auto get(const IndexType& idx) const
        {
            return Any<__TypeList, make_type_t>(
                const_cast<void*>(__UnderlyingType::__pointer__(static_cast<std::size_t>(idx))), idx);
        }

        /**
        * @brief Get by type.
        * @tparam S Type of which the storage will be retrieved.
        */
        template<typename S, typename = std::enable_if_t<__TypeList::template contains_v<S>>>
        auto& get()
        {
            static_assert(__TypeList::template contains_v<S>, "Type is not listed.");
            using __TargetType = ContainerTypes<__TypeList, __TypeList::template index_of_v<S>, nptr>;
            return reinterpret_cast<__TargetType*>(this)->__data;
        }

        template<typename S, typename = std::enable_if_t<__TypeList::template contains_v<S>>>
        const auto& get() const
        {
            static_assert(__TypeList::template contains_v<S>, "Type is not listed.");
            using __TargetType = ContainerTypes<__TypeList, __TypeList::template index_of_v<S>, nptr, Prototype>;
            return reinterpret_cast<const __TargetType*>(this)->__data;
        }

        /**
        * @brief Get by index.
        * @tparam IndexType Index of which the storage will be retrieved.
        */
        template<IndexType idx, typename = std::enable_if_t<__TypeList::template within_bounds_v<idx>>>
        auto& get()
        {
            static_assert(__TypeList::template within_bounds_v<idx>, "Type index out of bounds.");
            using __TargetType = ContainerTypes<__TypeList, static_cast<std::size_t>(idx), nptr, Prototype>;
            return reinterpret_cast<__TargetType*>(this)->__data;
        }

        template<IndexType idx, typename = std::enable_if_t<__TypeList::template within_bounds_v<idx>>>
        const auto& get() const
        {
            static_assert(__TypeList::template within_bounds_v<idx>, "Type index out of bounds.");
            using __TargetType = ContainerTypes<__TypeList, static_cast<std::size_t>(idx), nptr, Prototype>;
            return reinterpret_cast<const __TargetType*>(this)->__data;
        }

        /**
        * Resets storage for all types.
        */
        virtual void clear()
        {
            __UnderlyingType::clear();
        }
        
    };

    


    template<typename __TypeList, std::size_t Idx, std::size_t nptr, template<typename> class Prototype, typename>
    class ContainerTypes
        : public ContainerTypes<__TypeList, Idx - 1U, nptr, Prototype>
    {
        static_assert(is_type_list_v<__TypeList>, "T must be a TypeList.");
        static_assert(!__TypeList::template has_duplicate_v<typename __TypeList::template Nth<Idx>>, "A type can only be listed once.");

    public:
        using Type = typename __TypeList::template Nth<Idx>;
        using __ThisType = ContainerTypes<__TypeList, Idx, nptr, Prototype>;
        using __NextType = ContainerTypes<__TypeList, Idx - 1, nptr, Prototype>;
        using __EvalType = make_ptr_t<Prototype<Type>, nptr>;

        __EvalType __data;

        ContainerTypes()
            : __NextType()
        {
            if constexpr (std::is_pointer_v<__EvalType>)
                __data = nullptr;
            else
                __data = {};
        }

        ContainerTypes(const __ThisType& other)
            : __NextType(static_cast<const __NextType&>(other))
        {
            if constexpr (std::is_pointer_v<__EvalType>)
            {
                auto ptr = other.__data;
                __data = ptr ? new __EvalType(*ptr) : nullptr;
            }
            else
            {
                __data = other.__data;
            }
        }

        ContainerTypes(__ThisType&& other)
            : __NextType(static_cast<__NextType&&>(std::forward<__ThisType>(other)))
        {
            __data = other.__data;
            if constexpr (std::is_pointer_v<__EvalType>)
                other.__data = nullptr;
            else
                other.__data = {};
        }

        virtual ~ContainerTypes()
        {
            if constexpr (std::is_pointer_v<__EvalType>)
                delete __data;
        }

        void operator=(const __ThisType& other)
        {
            if constexpr (std::is_pointer_v<__EvalType>)
            {
                delete __data;
                __data = other.__data ? new __EvalType(*other.__data) : nullptr;
                __NextType::operator=(static_cast<const __NextType&>(other));
            }
            else
            {
                __data = other.__data;
            }
        }

        bool operator==(const __ThisType& other) const
        {
            return ((std::is_pointer_v<__EvalType> && __data && other.__data && *__data == *other.__data)
                    || __data == other.__data)
                && __NextType::operator==(static_cast<const __NextType&>(other));
        }
    
    protected:
        virtual void clear()
        {
            if constexpr (std::is_pointer_v<__EvalType>)
            {
                delete __data;
                __data = nullptr;
            }
            else 
            {
                __data.~__EvalType();
                __data = {};
            }
            __NextType::clear();
        }
     
        inline void* __pointer__(const std::size_t& i)
        {
            if (i == Idx)
                return reinterpret_cast<void*>(&__data);
            else
                return __NextType::template __pointer__(i);
        }

        inline const void* __pointer__(const std::size_t& i) const
        {
            if (i == Idx)
                return reinterpret_cast<const void*>(&__data);
            else
                return __NextType::template __pointer__(i);
        }

    };

    template<typename __TypeList, std::size_t Idx, std::size_t nptr, template<typename> class Prototype>
    class ContainerTypes<__TypeList, Idx, nptr, Prototype, typename std::enable_if<Idx == 0U>::type>
    {
    public:
        using __ThisType = ContainerTypes<__TypeList, Idx, nptr, Prototype>;

        using Type = typename __TypeList::template Nth<Idx>;

        ContainerTypes() 
        {}

        ContainerTypes(const __ThisType& other)
        {}

        virtual ~ContainerTypes() 
        {}

        void operator=(const __ThisType& other) 
        {}
        
        bool operator==(const __ThisType& other) const
        {
            return true;
        }

    protected:
        virtual void clear()
        {}

        inline void* __pointer__(const std::size_t& i)
        {
            return nullptr;
        }

        inline const void* __pointer__(const std::size_t& i) const
        {
            throw std::runtime_error("Invalid enum");
        }

    };

}