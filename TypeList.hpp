#pragma once
#include <type_traits>
#include <tuple>
#include <vector>

namespace generic
{

    class NullType {};

    template<typename ...Args>
    class TypeList;

    template<typename EnumType, EnumType first, EnumType last, typename ...Args>
    class EnumIndexedTypeList;

    template<typename T>
    struct underlying_pointer_type;

    template<typename T, std::size_t n>
    struct make_ptr;

    template<typename ...Args>
    class TypeList
    {
    public:
        using IndexType = std::size_t;
        using Types = std::tuple<NullType, Args...>;
        static constexpr const std::size_t Size = sizeof...(Args);

        template <std::size_t N>
        using Nth = typename std::tuple_element<N, Types>::type;
        //using Nth = std::conditional_t<N == 0U, NullType, typename std::tuple_element<N - 1U, Types>::type>;

        template<typename T, std::size_t idx = Size>
        struct contains;

        template<typename T, std::size_t idx = Size, typename = void>
        struct index_of;

        template<typename T, bool in = false, std::size_t idx = Size>
        struct has_duplicate;

        template<typename T, std::size_t idx>
        struct contains
            : std::conditional_t
            < std::is_same_v<T, Nth<idx>>
            , std::true_type
            , contains<T, idx - 1U>
            >
        {};

        template<typename T>
        struct contains<T, 0U>
            : std::false_type
        {};

        template<typename T, std::size_t idx>
        struct index_of<T, idx, std::enable_if_t<contains<T>::value>>
            : std::conditional_t
            < std::is_same_v<T, Nth<idx>>
            , std::integral_constant<std::size_t, idx>
            , index_of<T, idx - 1U, std::enable_if_t<contains<T>::value>>
            >
        {
            static_assert(contains<T>::value, "TypeList does not contain T.");
        };

        template<typename T, bool in, std::size_t idx>
        struct has_duplicate
            : std::conditional_t
            < std::is_same_v<T, Nth<idx>>&& in
            , std::true_type
            , has_duplicate<T, std::is_same_v<T, Nth<idx>> || in, idx - 1U>
            >
        {
            static_assert(contains<T>::value, "TypeList does not contain T.");
        };

        template<typename T, bool in>
        struct has_duplicate<T, in, 0U>
            : std::false_type
        {};

        template<IndexType idx>
        struct within_bounds
            : std::conditional_t
            < idx >= 0 && idx <= Size
            , std::true_type
            , std::false_type
            >
        {};

        template<typename T>
        static constexpr const bool contains_v = contains<T>::value;

        template<typename T>
        static constexpr const std::size_t index_of_v = index_of<T>::value;

        template<typename T>
        static constexpr const bool has_duplicate_v = has_duplicate<T>::value;

        template<IndexType idx>
        static constexpr const bool within_bounds_v = within_bounds<idx>::value;

    };

    template<typename EnumType, EnumType _first, EnumType _last, typename ...Args>
    class EnumIndexedTypeList
        : public std::enable_if_t<
        (static_cast<std::underlying_type_t<EnumType>>(_last) - static_cast<std::underlying_type_t<EnumType>>(_first) + 1U) == TypeList<Args...>::Size,
        TypeList<Args...>
        >
    {
    private:
        static_assert(std::is_enum<EnumType>::value, "EnumType must have enum class qualifier.");

    public:
        using IndexType = EnumType;
        static constexpr const EnumType first = _first;
        static constexpr const EnumType last = _last;
        template<EnumType N>
        using TypeOf = typename TypeList<Args...>::template Nth<static_cast<std::size_t>(N)>;

    };
    
    template<typename T>
    struct is_type_list
        : std::false_type
    {};

    template<typename T, typename...Args>
    struct is_type_list<TypeList<T, Args...>>
        : std::true_type
    {};
    
    template<typename T, std::size_t n>
    struct make_ptr : make_ptr<std::add_pointer_t<T>, n - 1>
    {};

    template<typename T>
    struct make_ptr<T, 0U>
        : std::type_identity<T>
    {};

    template<typename T, std::size_t idx>
    using make_ptr_t = make_ptr<T, idx>::type;

    template<typename T>
    static constexpr const bool is_type_list_v = is_type_list<T>::value;

    template<typename __TypeList, typename __Base, std::size_t idx, typename = void>
    struct __is_common_base_ty
        : std::conditional_t
        < std::is_base_of_v<__Base, typename __TypeList::template Nth<idx>>
        , __is_common_base_ty<__TypeList, __Base, idx - 1U>
        , std::false_type
        >
    {};

    template<typename __TypeList, typename __Base, std::size_t idx>
    struct __is_common_base_ty<__TypeList, __Base, idx, std::enable_if_t<idx == 0U>>
        : std::true_type
    {};

    template<typename __TypeList, typename __Base, typename = void>
    struct __is_common_base
        : std::false_type
    {};

    template<typename __TypeList, typename __Base>
    struct __is_common_base<__TypeList, __Base, std::enable_if_t<is_type_list_v<__TypeList>>>
        : __is_common_base_ty<__TypeList, __Base, __TypeList::Size>
    {};

    template<typename __TypeList, typename __Base>
    struct is_common_base
        : __is_common_base<__TypeList, __Base>
    {};
    
    template<typename __TypeList, typename __Base>
    static constexpr const bool is_common_base_v = is_common_base<__TypeList, __Base>::value;

}