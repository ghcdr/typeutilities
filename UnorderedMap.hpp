#pragma once
#include "TypeList.hpp"
#include "Container.hpp"
#include <unordered_map>

namespace generic
{
    template<typename T>
    struct __UnorderedMap__
    {
        template<typename U>
        using prototype = std::unordered_map<T, U>;
    };

    /**
    * @brief Container for unordered maps.
    */
    template<typename KeyType, typename __TypeList, typename __Base = NullType, std::size_t nptr = 1U>
    class UnorderedMap;

    template<typename KeyType, typename __TypeList, typename __Base, std::size_t nptr>
    class UnorderedMap
        : public Container<__TypeList, typename __TypeList::IndexType, nptr, __UnorderedMap__<KeyType>::prototype, __Base>
    {
    private:
        using __ThisType = UnorderedMap<KeyType, __TypeList, __Base, nptr>;
        using __UnderlyingType = Container<__TypeList, typename __TypeList::IndexType, nptr, __UnorderedMap__<KeyType>::prototype, __Base>;

    public:
        UnorderedMap()
            : __UnderlyingType()
        {}

        UnorderedMap(const __ThisType& other)
            : __UnderlyingType(other)
        {}

        UnorderedMap(__ThisType&& other) noexcept
            : __UnderlyingType(static_cast<__UnderlyingType&&>(std::forward<__ThisType>(other)))
        {}

        virtual ~UnorderedMap()
        {}

        __ThisType& operator=(const __ThisType& other)
        {
            __UnderlyingType::operator=(static_cast<const __UnderlyingType&>(other));
            return *this;
        }

        bool operator==(const __ThisType& other)
        {
            return __UnderlyingType::operator==(static_cast<const __UnderlyingType&>(other));
        }

        void clear()
        {
            __UnderlyingType::clear();
        }
    };

}