#pragma once
#include "TypeList.hpp"
#include "Container.hpp"
#include <map>

namespace generic
{

    template<typename T>
    struct __Map__
    {
        template<typename U>
        using prototype = std::map<T, U>;
    };

    /**
    * @brief Container for maps.
    */
    template<typename KeyType, typename __TypeList, typename __Base = NullType, std::size_t nptr = 1U>
    class Map;

    template<typename KeyType, typename __TypeList, typename __Base, std::size_t nptr>
    class Map
        : public Container<__TypeList, typename __TypeList::IndexType, nptr, __Map__<KeyType>::prototype, __Base>
    {
    private:
        using __ThisType = Map<KeyType, __TypeList, __Base, nptr>;
        using __UnderlyingType = Container<__TypeList, typename __TypeList::IndexType, nptr, __Map__<KeyType>::prototype, __Base>;

    public:
        Map()
            : __UnderlyingType()
        {}

        Map(const __ThisType& other)
            : __UnderlyingType(other)
        {}

        Map(__ThisType&& other) noexcept
            : __UnderlyingType(static_cast<__UnderlyingType&&>(std::forward<__ThisType>(other)))
        {}

        virtual ~Map()
        {}

        __ThisType& operator=(const __ThisType& other)
        {
            __UnderlyingType::operator=(static_cast<const __UnderlyingType&>(other));
            return *this;
        }

        bool operator==(const __ThisType& other)
        {
            return __UnderlyingType::operator==(static_cast<const __UnderlyingType&>(other));
        }

        void clear()
        {
            __UnderlyingType::clear();
        }
    };

}