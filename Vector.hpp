#pragma once
#include "TypeList.hpp"
#include "Container.hpp"
#include <vector>

namespace generic
{
    /**
    * @brief Container for vectors.
    */
    template<typename __TypeList, typename __Base = NullType, std::size_t nptr = 1U>
    class Vector;

    template<typename __TypeList, typename __Base, std::size_t nptr>
    class Vector
        : public Container<__TypeList, typename __TypeList::IndexType, nptr, std::vector, __Base>
    {
    private:
        using __ThisType = Vector<__TypeList, __Base, nptr>;
        using __UnderlyingType = Container<__TypeList, typename __TypeList::IndexType, nptr, std::vector, __Base>;

    public:
        Vector()
            : __UnderlyingType()
        {}

        Vector(const __ThisType& other)
            : __UnderlyingType(other)
        {}

        Vector(__ThisType&& other) noexcept
            : __UnderlyingType(static_cast<__UnderlyingType&&>(std::forward<__ThisType>(other)))
        {}

        virtual ~Vector()
        {}

        __ThisType& operator=(const __ThisType& other)
        {
            __UnderlyingType::operator=(static_cast<const __UnderlyingType&>(other));
            return *this;
        }

        bool operator==(const __ThisType& other)
        {
            return __UnderlyingType::operator==(static_cast<const __UnderlyingType&>(other));
        }

        void clear()
        {
            __UnderlyingType::clear();
        }

    };

}